Blank template layout of the site.

### Used ###
* **Css:** *Bootstrap, SASS, Compass.*
* **Js:** *fancybox, jquery.tools, jquery.formstyler.*

[Wiki](https://bitbucket.org/t0H/clear-template/wiki)